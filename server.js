const express = require("express");
const nunjucks = require("nunjucks");

const routes = require("./routes");

const app = express();
const PORT = process.env.PORT || 3000;

// nunjucks setup
app.set("view engine", "html");
nunjucks.configure(["views/"],{
    autoescape: false,
    express: app
});

//when routes are called, go to ./routes folder
app.use(routes);

// start server on given PORT
app.listen(PORT, () => {
    console.log(`server listening on ${PORT}`)
});